package com.tw.employee.controller;

import com.tw.employee.entity.Employee;
import com.tw.employee.repository.EmployeeRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/employees")
public class EmployeeController {

    private EmployeeRepository employeeRepository;

    @GetMapping
    public List<Employee> getAllEmployees() {
        log.info("Getting all employees");
        return employeeRepository.findAll();
    }

    @GetMapping("/by-dept-id/{departmentId}")
    public List<Employee> getEmployeesByDepartmentId(@PathVariable Integer departmentId) {
        log.info("Getting employees by dept id {}", departmentId);
        return employeeRepository.findByDepartmentId(departmentId);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public List<Employee> addEmployees(@RequestBody List<Employee> employees) {
        log.info("Saving employees: {}", employees.size());
        return employeeRepository.saveAll(employees);
    }

    @Transactional
    @DeleteMapping("/by-dept-id/{departmentId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteEmployee(@PathVariable Integer departmentId) {
        log.info("Deleting employee with dept id: {}", departmentId);
        employeeRepository.deleteByDepartmentId(departmentId);
    }
}
