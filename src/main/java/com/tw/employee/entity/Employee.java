package com.tw.employee.entity;

import lombok.Getter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Getter
public class Employee {

    @Id
    @GeneratedValue
    private Integer employeeId;

    private String name;

    private Integer departmentId;

}
