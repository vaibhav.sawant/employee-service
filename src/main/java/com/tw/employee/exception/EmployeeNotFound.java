package com.tw.employee.exception;

public class EmployeeNotFound extends RuntimeException {
    public EmployeeNotFound(Integer employeeId) {
        super(String.format("Employee not found with id %d", employeeId));
    }
}
