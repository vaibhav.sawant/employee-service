package com.tw.employee.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.HashMap;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(EmployeeNotFound.class)
    public ResponseEntity<HashMap<String, Object>> handleEmployeeNotFound(EmployeeNotFound employeeNotFound) {

        HashMap<String, Object> errorMap = new HashMap<>();
        errorMap.put("message", employeeNotFound.getMessage());
        errorMap.put("errorCode", HttpStatus.NOT_FOUND.value());

        return ResponseEntity.badRequest().body(errorMap);
    }
}
