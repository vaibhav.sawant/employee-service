package com.tw.employee.repository;

import com.tw.employee.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

    List<Employee> findByDepartmentId(Integer departmentId);

    void deleteByDepartmentId(Integer departmentId);
}
